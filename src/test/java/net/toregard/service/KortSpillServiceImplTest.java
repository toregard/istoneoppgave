package net.toregard.service;

import net.toregard.Application;
import net.toregard.fault.BusinessFault;
import net.toregard.model.Kort;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.Assert;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class KortSpillServiceImplTest {
    @Autowired
    KortSpillService kortSpillService;

    @Rule
    public ExpectedException expectedThrown = ExpectedException.none();


    @Test
    public void  autowiredKortKortSpillServiceTest(){
        Assert.assertNotNull(kortSpillService);
     }

    @Test
    public void sorterBusinessFaultTest()throws BusinessFault{
        expectedThrown.expect(BusinessFault.class);
        expectedThrown.expectMessage(BusinessFault.SORTER_VALIDERING_KORTNUMMERID_MELDING);
        kortSpillService.sorter(0);
    }

    @Test
    public void trekkBusinessFaultTest() throws BusinessFault{
        expectedThrown.expect(BusinessFault.class);
        expectedThrown.expectMessage(BusinessFault.TREKK_VALIDERING_KORTNUMMERID_MELDING);
        kortSpillService.trekk(0);
    }

    @Test
    public void stokkBusinessFaultTest() throws BusinessFault{
        expectedThrown.expect(BusinessFault.class);
        expectedThrown.expectMessage(BusinessFault.STOKK_VALIDERING_KORTNUMMERID_MELDING);
        kortSpillService.stokk(0);
    }

    @Test
    public void trekkTest()throws BusinessFault{
        int KORTNUMMER=1;
        int HJERTE=0,SPAR=3;
        int ESS=0,KONGE=12;
        int ANTALLKORT=52;
        //Trekk forste kort som er Klover Konge
        Kort kloverKonge =kortSpillService.trekk(KORTNUMMER);
        Assert.assertEquals(SPAR,kloverKonge.getType());
        Assert.assertEquals(KONGE,kloverKonge.getVerdi());
        //Sorter
        List<Kort> kortStokk =kortSpillService.sorter(KORTNUMMER);
        //Antall kort igjen er 51 av 52
        Assert.assertEquals(ANTALLKORT-1,kortStokk.size());
        //Hjerte ess ligger på topp
        Assert.assertEquals(kortStokk.get(0).getType(),HJERTE);
        Assert.assertEquals(kortStokk.get(0).getType(),ESS);
        //Trekk nytt kort. Hjerte ess trekkes.
        Kort hjerteEss=kortSpillService.trekk(KORTNUMMER);
        Assert.assertEquals(HJERTE,hjerteEss.getType());
        Assert.assertEquals(0,hjerteEss.getVerdi());
        //Hjerter 2 ligger på topp
        Kort hjerte2=kortSpillService.trekk(KORTNUMMER);
        Assert.assertEquals(HJERTE,hjerte2.getType());
        Assert.assertEquals(1,hjerte2.getVerdi());
        //tre kort er trukket
        Assert.assertEquals(ANTALLKORT-3,kortStokk.size());
        for(int i=0; i<ANTALLKORT-3; i++) kortSpillService.trekk(KORTNUMMER);
        Assert.assertEquals(0,kortStokk.size());

        try{
            Kort nykortstokk = kortSpillService.trekk(KORTNUMMER);
            Assert.assertTrue(false);
        }catch(BusinessFault e){
           Assert.assertEquals(BusinessFault.TREKK_EXCEPTION_KORTNUMMERID_KODE,e.getId());
        }

        try{
            kortSpillService.sorter(KORTNUMMER);
            Assert.assertTrue(false);
        }catch(BusinessFault e){
            Assert.assertEquals(BusinessFault.SORTER_EMPTY_KORT_KODE,e.getId());
        }

        try{
            kortSpillService.stokk(KORTNUMMER);
            Assert.assertTrue(false);
        }catch(BusinessFault e){
            Assert.assertEquals(BusinessFault.STOKK_EMPTY_KORT_KODE,e.getId());
        }
    }

}
