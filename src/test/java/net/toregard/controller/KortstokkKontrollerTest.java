package net.toregard.controller;

import net.toregard.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class KortstokkKontrollerTest {
    @Value("${local.server.port}")
    int port;
    final RestTemplate template = new RestTemplate();

    @Test
    public void funksjonsTest() {
        /**
         * Forste gang du trekker lage en kortstokk og Spar Konge ligger øverst
         */

        //Trekk 1
        StringBuffer url= new StringBuffer().
        append("http://localhost:").append(port).append("/kortspill/trekkekort/1");
        KortViewerImpl message = template.getForObject(url.toString(), KortViewerImpl.class);
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
        Assert.assertEquals(message.getKort().getFarge(),"Spar");
        Assert.assertEquals(message.getKort().getVerdi(),"Konge");

        //Sorter
        url= new StringBuffer().append("http://localhost:").append(port).append("/kortspill/sorter/1");
        KortStokkViewerImpl messageListe = template.getForObject(url.toString(), KortStokkViewerImpl.class);
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
        Assert.assertEquals(messageListe.getKortStokk().get(0).getFarge(),"Hjerter");
        Assert.assertEquals(messageListe.getKortStokk().get(0).getVerdi(),"ess");
        Assert.assertEquals(messageListe.getKortStokk().get(1).getFarge(),"Hjerter");
        Assert.assertEquals(messageListe.getKortStokk().get(1).getVerdi(),"2");
        //Trekk 2
        url= new StringBuffer().append("http://localhost:").append(port).append("/kortspill/trekkekort/1");
        message = template.getForObject(url.toString(), KortViewerImpl.class);
        Assert.assertNotNull(message);
        Assert.assertNotNull(message.getKort());
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
        Assert.assertEquals(message.getKort().getFarge(),"Hjerter");
        Assert.assertEquals(message.getKort().getVerdi(),"ess");
        //Trekk 3
        url= new StringBuffer().append("http://localhost:").append(port).append("/kortspill/trekkekort/1");
        message = template.getForObject(url.toString(), KortViewerImpl.class);
        Assert.assertNotNull(message);
        Assert.assertNotNull(message.getKort());
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
        Assert.assertEquals(message.getKort().getFarge(),"Hjerter");
        Assert.assertEquals(message.getKort().getVerdi(),"2");
        //Stokk
        url= new StringBuffer().append("http://localhost:").append(port).append("/kortspill/stokk/1");
        messageListe = template.getForObject(url.toString(), KortStokkViewerImpl.class);
        Assert.assertNotNull(message);
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
        Assert.assertNotNull(messageListe.getKortStokk());
        Assert.assertEquals(messageListe.getKortStokk().size(),49);
        Assert.assertNotEquals(messageListe.getKortStokk().get(0).getFarge(),"Hjerter");
        Assert.assertNotEquals(messageListe.getKortStokk().get(0).getVerdi(),"2");
    }

    //@Test
    public void stokk() {
        StringBuffer url= new StringBuffer().
                append("http://localhost:").
                append(port).
                append("/kortspill/stokk/1");
        String a=url.toString();
        KortStokkViewerImpl message = template.getForObject(url.toString(), KortStokkViewerImpl.class);
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
    }

    //@Test
    public void sorter() {
        StringBuffer url= new StringBuffer().
                append("http://localhost:").
                append(port).
                append("/kortspill/sorter/1");
        String a=url.toString();
        KortStokkViewerImpl message = template.getForObject(url.toString(), KortStokkViewerImpl.class);
        Assert.assertEquals(0, message.getId());
        Assert.assertEquals("Ok", message.getMessage());
    }
}
