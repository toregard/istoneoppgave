package net.toregard.transform;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TransformKortImplTest {
    private final net.toregard.model.Kort n;
    private final net.toregard.controller.Kort expected;
    private static final String[] farge = {"Hjerter", "Kløver", "Ruter", "Spar"};
    private static final String[] verdi = {"ess", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Knekt", "Dronning", "Konge"};
    TransformKortImpl transformKortImpl = null;

    public TransformKortImplTest(net.toregard.model.Kort n, net.toregard.controller.Kort expected) {
        this.n = n;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {createModelKort(0, 0), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[0])},
                {createModelKort(0, 1), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[1])},
                {createModelKort(0, 2), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[2])},
                {createModelKort(0, 3), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[3])},
                {createModelKort(0, 4), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[4])},
                {createModelKort(0, 5), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[5])},
                {createModelKort(0, 6), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[6])},
                {createModelKort(0, 7), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[7])},
                {createModelKort(0, 8), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[8])},
                {createModelKort(0, 9), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[9])},
                {createModelKort(0, 10), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[10])},
                {createModelKort(0, 11), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[11])},
                {createModelKort(0, 12), createControllerKort(TransformKortImpl.farge[0], TransformKortImpl.verdi[12])},

                {createModelKort(1, 0), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[0])},
                {createModelKort(1, 1), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[1])},
                {createModelKort(1, 2), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[2])},
                {createModelKort(1, 3), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[3])},
                {createModelKort(1, 4), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[4])},
                {createModelKort(1, 5), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[5])},
                {createModelKort(1, 6), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[6])},
                {createModelKort(1, 7), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[7])},
                {createModelKort(1, 8), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[8])},
                {createModelKort(1, 9), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[9])},
                {createModelKort(1, 10), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[10])},
                {createModelKort(1, 11), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[11])},
                {createModelKort(1, 12), createControllerKort(TransformKortImpl.farge[1], TransformKortImpl.verdi[12])},

                {createModelKort(2, 0), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[0])},
                {createModelKort(2, 1), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[1])},
                {createModelKort(2, 2), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[2])},
                {createModelKort(2, 3), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[3])},
                {createModelKort(2, 4), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[4])},
                {createModelKort(2, 5), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[5])},
                {createModelKort(2, 6), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[6])},
                {createModelKort(2, 7), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[7])},
                {createModelKort(2, 8), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[8])},
                {createModelKort(2, 9), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[9])},
                {createModelKort(2, 10), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[10])},
                {createModelKort(2, 11), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[11])},
                {createModelKort(2, 12), createControllerKort(TransformKortImpl.farge[2], TransformKortImpl.verdi[12])},

                {createModelKort(3, 0), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[0])},
                {createModelKort(3, 1), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[1])},
                {createModelKort(3, 2), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[2])},
                {createModelKort(3, 3), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[3])},
                {createModelKort(3, 4), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[4])},
                {createModelKort(3, 5), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[5])},
                {createModelKort(3, 6), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[6])},
                {createModelKort(3, 7), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[7])},
                {createModelKort(3, 8), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[8])},
                {createModelKort(3, 9), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[9])},
                {createModelKort(3, 10), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[10])},
                {createModelKort(3, 11), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[11])},
                {createModelKort(3, 12), createControllerKort(TransformKortImpl.farge[3], TransformKortImpl.verdi[12])},
       });
    }

    @Before
    public void initialize() {
        transformKortImpl = new TransformKortImpl();
    }

    @Test
    public void test() {
        net.toregard.controller.Kort actally = transformKortImpl.transform(n);
        Assert.assertEquals(expected, actally);
    }

    private static net.toregard.model.Kort createModelKort(int farge, int verdi) {
        return new net.toregard.model.Kort(farge, verdi);
    }

    private static net.toregard.controller.Kort createControllerKort(String farge, String verdi) {
        return new net.toregard.controller.Kort(farge, verdi);
    }
}
