package net.toregard.repository;

import net.toregard.Application;
import net.toregard.repository.KortStokker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class KortStokkerRepositoryTest {
    @Autowired
    @Qualifier("kortStokkerRepository")
    KortStokker kortStokker;

    @Test
    public void autowiredKortStokkerTest(){
        Assert.notNull(kortStokker);
    }
}
