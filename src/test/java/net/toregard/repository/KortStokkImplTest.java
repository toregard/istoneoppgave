package net.toregard.repository;

import net.toregard.model.Kort;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class KortStokkImplTest {
    private KortStokkImpl kortStokk;
    final int ANTALL_KORT = 52;
    final int ANTALL_KORTTYPER = 4;
    final int ANTALL_VERDIER = 13;

    @Before
    public void setUp() {
        kortStokk = new KortStokkImpl();
    }

    @Test
    public void constructorTest() {
        Assert.assertEquals("Antall Kort skal være "+Integer.toString(ANTALL_KORT), ANTALL_KORT, kortStokk.getList().size());
    }

    @Test
    public void draKortTest() {
        while(kortStokk.getList().size()>0){
            for (int type = ANTALL_KORTTYPER-1; type >= 0; type--) {
                for (int kortNr = ANTALL_VERDIER-1; kortNr >= 0; kortNr--) {
                    Assert.assertEquals(type, kortStokk.getDeque().peek().getType());
                    Assert.assertEquals(kortNr, kortStokk.getDeque().peek().getVerdi());
                    kortStokk.trekk();
                }
            }
            Assert.assertEquals(0,kortStokk.getList().size());
        }
     }

    @Test
    public void stokkKortTest() {
        LinkedList<Kort> _kortStokk = (LinkedList<Kort>) ( (LinkedList<Kort>)kortStokk.getList() ).clone();
        for (int kortNr = 0; kortNr <= _kortStokk.size()-1; kortNr++) {
            Assert.assertEquals(_kortStokk.get(kortNr).getType(), kortStokk.getList().get(kortNr).getType());
            Assert.assertEquals(_kortStokk.get(kortNr).getVerdi(), kortStokk.getList().get(kortNr).getVerdi());
        }
       kortStokk.stokk();
       int feil=0;
       for (int kortNr = 0; kortNr <= _kortStokk.size()-1; kortNr++) {
          if(_kortStokk.get(kortNr).getType()!= kortStokk.getList().get(kortNr).getType()
                  ||
             _kortStokk.get(kortNr).getVerdi() != kortStokk.getList().get(kortNr).getVerdi()
                  ){ feil++; continue;
         }
       }
       Assert.assertTrue("Ingen effekt av stokkingen",feil>0);
     }

    @Test
    public void sorterKortTest() {
        kortStokk.stokk();
        kortStokk.sorter();
        LinkedList<Kort> _kortStokk = (LinkedList<Kort>) ( (LinkedList<Kort>)kortStokk.getList() ).clone();
        kortStokk.stokk();
        Assert.assertNotEquals(_kortStokk.toArray(),kortStokk.getList().toArray());
        kortStokk.sorter();
        Assert.assertArrayEquals(_kortStokk.toArray(),kortStokk.getList().toArray());
    }
}
