package net.toregard.controller;


public abstract class Viewer {
    private int id;
    private String message;
    private Integer kortStokkId;

    public Viewer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getKortStokkId() {
        return kortStokkId;
    }

    public void setKortStokkId(Integer kortStokkId) {
        this.kortStokkId = kortStokkId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Viewer viewer = (Viewer) o;

        if (id != viewer.id) return false;
        if (message != null ? !message.equals(viewer.message) : viewer.message != null) return false;
        return kortStokkId != null ? kortStokkId.equals(viewer.kortStokkId) : viewer.kortStokkId == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (kortStokkId != null ? kortStokkId.hashCode() : 0);
        return result;
    }
}
