package net.toregard.controller;

import java.util.Objects;

public class Kort {
    private String farge;
    private String verdi;

    public Kort() {
    }

    public Kort(String farge, String verdi){
        super();
        this.farge=farge;
        this.verdi=verdi;
    }

    public String getFarge() {
        return farge;
    }

    public void setFarge(String farge) {
        this.farge = farge;
    }

    public String getVerdi() {
        return verdi;
    }

    public void setVerdi(String verdi) {
        this.verdi = verdi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kort kort = (Kort) o;
        return Objects.equals(getFarge(), kort.getFarge()) &&
                Objects.equals(getVerdi(), kort.getVerdi());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFarge(), getVerdi());
    }
}
