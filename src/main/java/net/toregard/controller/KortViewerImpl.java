package net.toregard.controller;

public class KortViewerImpl extends Viewer{
    private Kort kort;

    public Kort getKort() {
        return kort;
    }

    public KortViewerImpl() {
     }

    public KortViewerImpl(Kort kort) {
        super();
        this.kort = kort;
    }

    public void setKort(Kort kort) {
        this.kort = kort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        KortViewerImpl that = (KortViewerImpl) o;

        return kort != null ? kort.equals(that.kort) : that.kort == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (kort != null ? kort.hashCode() : 0);
        return result;
    }
}
