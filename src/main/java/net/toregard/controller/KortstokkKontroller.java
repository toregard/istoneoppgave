package net.toregard.controller;

import net.toregard.fault.BusinessFault;
import net.toregard.service.KortSpillService;
import net.toregard.transform.TransformKortImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
@RequestMapping("/kortspill")
public class KortstokkKontroller {

    @Autowired
    KortSpillService kortSpillService;

    @Autowired
    TransformKortImpl transformKortImpl;

    /**
     * Eks.: http://localhost:8080/kortspill/stokk/0
     * @param kortStokkId
     * @return
     */
    @RequestMapping(value = "/stokk/{kortStokkId}",method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<KortStokkViewerImpl> stokk(@PathVariable("kortStokkId") Integer kortStokkId) {
        KortStokkViewerImpl kortStokkViewer = new KortStokkViewerImpl();
        HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
        try{
            List<net.toregard.model.Kort> kortStokk=kortSpillService.stokk(kortStokkId);
            kortStokkViewer.setKortStokk(transformKortImpl.listTransform(kortStokk));
            kortStokkViewer.setKortStokkId(kortStokkId);
            kortStokkViewer.setId(0);
            kortStokkViewer.setMessage("Ok");
            httpStatus=HttpStatus.OK;
        }catch (BusinessFault businessFault){
            kortStokkViewer.setId(businessFault.getId());
            kortStokkViewer.setMessage(businessFault.getMessage());
            httpStatus=HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<KortStokkViewerImpl>(kortStokkViewer,httpStatus);
    }

    /**
     * Eks.: http://localhost:8080/kortspill/sorter/0
     * @param kortStokkId
     * @return
     */
    @RequestMapping(value = "/sorter/{kortStokkId}",method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<KortStokkViewerImpl> sorter(@PathVariable("kortStokkId") int kortStokkId) {
        KortStokkViewerImpl kortStokkViewer = new KortStokkViewerImpl();
        HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
        try{
            List<net.toregard.model.Kort> kortStokk=kortSpillService.sorter(kortStokkId);
            kortStokkViewer.setKortStokk(transformKortImpl.listTransform(kortStokk));
            kortStokkViewer.setKortStokkId(kortStokkId);
            kortStokkViewer.setMessage("Ok");
            httpStatus=HttpStatus.OK;
        }catch (BusinessFault businessFault){
            kortStokkViewer.setId(businessFault.getId());
            kortStokkViewer.setMessage(businessFault.getMessage());
            httpStatus=HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<KortStokkViewerImpl>(kortStokkViewer,httpStatus);
    }

    /**
     * Eks.: http://localhost:8080/kortspill/trekkekort/0
     * @param kortStokkId
     * @return
     */
    @RequestMapping(value = "/trekkekort/{kortStokkId}",method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<KortViewerImpl> trekkeKort(@PathVariable("kortStokkId") int kortStokkId) {
        KortViewerImpl kortViewer = new KortViewerImpl();
        HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
        try{
            kortViewer.setKort(transformKortImpl.transform(kortSpillService.trekk(kortStokkId)));
            kortViewer.setKortStokkId(kortStokkId);
            kortViewer.setId(0);
            kortViewer.setMessage("Ok");
            httpStatus=HttpStatus.OK;
        }catch (BusinessFault businessFault){
            kortViewer.setId(businessFault.getId());
            kortViewer.setMessage(businessFault.getMessage());
            httpStatus=HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<KortViewerImpl>(kortViewer,httpStatus);
    }
}
