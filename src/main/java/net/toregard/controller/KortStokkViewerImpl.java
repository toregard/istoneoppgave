package net.toregard.controller;

import java.util.List;

public class KortStokkViewerImpl extends Viewer{
    List<Kort> kortStokk;

    public  List<Kort> getKortStokk() {
        return kortStokk;
    }

    public void setKortStokk( List<Kort> kortStokk) {
        this.kortStokk = kortStokk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        KortStokkViewerImpl that = (KortStokkViewerImpl) o;

        return kortStokk != null ? kortStokk.equals(that.kortStokk) : that.kortStokk == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (kortStokk != null ? kortStokk.hashCode() : 0);
        return result;
    }
}
