package net.toregard.service;

import net.toregard.fault.BusinessFault;
import net.toregard.model.Kort;

import net.toregard.repository.KortStokker;
import net.toregard.validering.Valider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KortSpillServiceImpl implements KortSpillService {
    @Autowired
    @Qualifier("kortStokkerRepository")
    KortStokker kortStokker;

    @Autowired
    @Qualifier("valideKortStokkId")
    Valider<Integer> valideKortStokkId;

    @Autowired
    @Qualifier("validerKortStokkTom")
    Valider<List<Kort>> validerKortStokkTom;

    @Override
    public List<Kort> stokk(Integer kortStokkId) throws BusinessFault {
        valideKortStokkId.valider(kortStokkId,BusinessFault.STOKK_VALIDERING_KORTNUMMERID_KODE,BusinessFault.STOKK_VALIDERING_KORTNUMMERID_MELDING);
        List<Kort> kortStokk = kortStokker.sorter(kortStokkId);
        validerKortStokkTom.valider(kortStokk,BusinessFault.STOKK_EMPTY_KORT_KODE,BusinessFault.TREKK_VALIDERING_KORTNUMMERID_MELDING);
        return kortStokker.stokk(kortStokkId);
    }

    @Override
    public List<Kort> sorter(Integer kortStokkId) throws BusinessFault {
        valideKortStokkId.valider(kortStokkId,BusinessFault.SORTER_VALIDERING_KORTNUMMERID_KODE,BusinessFault.SORTER_VALIDERING_KORTNUMMERID_MELDING);
        List<Kort> kortStokk = kortStokker.sorter(kortStokkId);
        validerKortStokkTom.valider(kortStokk,BusinessFault.SORTER_EMPTY_KORT_KODE,BusinessFault.SORTER_EMPTY_KORT_MELDING);
        return kortStokk;
    }

    @Override
    public Kort trekk(Integer kortStokkId) throws BusinessFault {
        valideKortStokkId.valider(kortStokkId,BusinessFault.TREKK_VALIDERING_KORTNUMMERID_KODE,BusinessFault.TREKK_VALIDERING_KORTNUMMERID_MELDING);
        Kort kortstokk =null;
        try{
            kortstokk =kortStokker.trekk(kortStokkId);

        }catch(Exception e){
            throw new BusinessFault(BusinessFault.TREKK_EXCEPTION_KORTNUMMERID_KODE,BusinessFault.TREKK_EXCEPTION_KORTNUMMERID_MELDING);
        }
        return kortstokk;
    }

}
