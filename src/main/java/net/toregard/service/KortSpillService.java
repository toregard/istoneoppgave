package net.toregard.service;

import net.toregard.fault.BusinessFault;
import net.toregard.model.Kort;

import java.util.List;

public interface KortSpillService {
    List<Kort> stokk(Integer kortStokkId) throws BusinessFault;
    List<Kort> sorter(Integer kortStokkId) throws BusinessFault;
    Kort trekk(Integer kortStokkId) throws BusinessFault;
}
