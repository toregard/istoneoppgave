package net.toregard.validering;

import net.toregard.fault.BusinessFault;

public interface Valider<T> {
    void valider(T t, int id, String melding) throws BusinessFault;
}
