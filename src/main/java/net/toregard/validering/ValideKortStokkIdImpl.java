package net.toregard.validering;

import net.toregard.fault.BusinessFault;
import org.springframework.stereotype.Service;

@Service("valideKortStokkId")
public class ValideKortStokkIdImpl implements Valider<Integer>{
    private final int VERDI_MIN=1;
    @Override
    public void valider(Integer verdi, int id, String melding) throws BusinessFault {
        if(verdi<VERDI_MIN ) throw new BusinessFault(id,melding);
    }
}
