package net.toregard.validering;

import net.toregard.fault.BusinessFault;
import net.toregard.model.Kort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("validerKortStokkTom")
public class ValiderKortStokkTom implements Valider<List<Kort>>{
    @Override
    public void valider(List<Kort> korts, int id, String melding) throws BusinessFault {
       if(korts==null || korts.isEmpty()) throw new BusinessFault(id,melding);
    }
}
