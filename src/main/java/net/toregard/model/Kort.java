package net.toregard.model;


import java.io.Serializable;

public class Kort  {

    /**
     * Type kort er 0=Konge,1=Ruter,2=Klover og 3=Spar (4 stk.)
     */
    private int type;

    /**
     * Mulige verdier er 0-12 (13 stk.) 0=A...Knekt=11,12=dronning,13=konge
     */
    private int verdi;


    public Kort(int type, int verdi) {
        this.type = type;
        this.verdi = verdi;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVerdi() {
        return verdi;
    }

    public void setVerdi(int verdi) {
        this.verdi = verdi;
    }

        @Override
    public String toString() {
        return "Kort{" +
                "type=" + type +
                ", verdi=" + verdi +
                '}';
    }
}
