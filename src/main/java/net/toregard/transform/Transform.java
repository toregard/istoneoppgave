package net.toregard.transform;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface Transform<K,T> {
    public K transform(T t);
    public List<K> listTransform(List<T> t);
}
