package net.toregard.transform;

import net.toregard.controller.Kort;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class TransformKortImpl implements Transform<net.toregard.controller.Kort, net.toregard.model.Kort> {
    public static String[] farge = {"Hjerter", "Ruter","Kløver", "Spar"};
    public static String[] verdi = {"ess","2", "3", "4", "5", "6", "7", "8", "9", "10", "Knekt", "Dronning", "Konge"};

    @Override
    public Kort transform(net.toregard.model.Kort kort) {
        return createKort(kort);
    }

    @Override
    public List<Kort> listTransform(List<net.toregard.model.Kort> kortListe) {
        List<Kort> _kortStokk = new ArrayList<Kort>();
        for (net.toregard.model.Kort kort : kortListe) {
            _kortStokk.add(createKort(kort));
        }
        return _kortStokk;
    }


    private Kort createKort(net.toregard.model.Kort kort){
        return new net.toregard.controller.Kort(farge[kort.getType()], verdi[kort.getVerdi()]);
    }


}
