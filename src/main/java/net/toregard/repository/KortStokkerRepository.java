package net.toregard.repository;


import net.toregard.model.Kort;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("kortStokkerRepository")
public class KortStokkerRepository implements KortStokker{
    HashMap<Integer,KortStokk> kortStokker;

    public KortStokkerRepository(){
        kortStokker = new HashMap<Integer,KortStokk>();
    }

    @Override
    public List<Kort> sorter(Integer kortStokkId) {
      return hentKortStokk(kortStokkId).sorter();
    }

    @Override
    public List<Kort> stokk(Integer kortStokkId) {
       return hentKortStokk(kortStokkId).stokk();
    }

    @Override
    public Kort trekk(Integer kortStokkId) {
        return hentKortStokk(kortStokkId).trekk();
    }


    private KortStokk hentKortStokk(Integer kortStokkId) {
       if(kortStokker.containsKey(kortStokkId))
          {
              return kortStokker.get(kortStokkId);
          }
        else
        {
            KortStokkImpl kortStokk =new KortStokkImpl();
            kortStokker.put(kortStokkId,kortStokk);
            return kortStokker.get(kortStokkId);
        }
    }

}
