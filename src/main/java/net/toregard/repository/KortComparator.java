package net.toregard.repository;

import net.toregard.model.Kort;
import java.util.Comparator;

public class KortComparator implements Comparator<Kort> {
    @Override
    public int compare(Kort o1, Kort o2) {
        if(o1.getType()>o2.getType()) return 1;
        else if(o1.getType()<o2.getType()) return -1;
        else{
            if(o1.getVerdi()>o2.getVerdi()) return 1;
            else  if(o1.getVerdi()<o2.getVerdi()) return -1;
        }
        return 0;
    }
}
