package net.toregard.repository;

import net.toregard.model.Kort;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("kortStokker2Repository")
public class KortStokker2Repository implements KortStokker{
    @Override
    public List<Kort> sorter(Integer kortStokkId) {
        List<Kort> kortStokk = new ArrayList<Kort>();
        kortStokk.add(0,createKort(0,0));
        kortStokk.add(0,createKort(1,0));
        kortStokk.add(0,createKort(2,0));
        kortStokk.add(0,createKort(3,0));
        return kortStokk;
    }

    @Override
    public List<Kort> stokk(Integer kortStokkId) {
        List<Kort> kortStokk = new ArrayList<Kort>();
        kortStokk.add(1,createKort(0,0));
        kortStokk.add(0,createKort(1,0));
        kortStokk.add(3,createKort(2,0));
        kortStokk.add(2,createKort(3,0));
        return kortStokk;
    }

    @Override
    public Kort trekk(Integer kortStokkId) {
        return createKort(0,1);
    }

    private List<Kort> createKorteStokk(){
        List<Kort> kortStokk = new ArrayList<Kort>();
        kortStokk.add(0,createKort(0,0));
        kortStokk.add(0,createKort(1,0));
        kortStokk.add(0,createKort(2,0));
        kortStokk.add(0,createKort(3,0));
        return kortStokk;
    }

    private Kort createKort(int farge, int verdi){
        return new Kort(farge,verdi);
    }
}
