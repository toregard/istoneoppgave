package net.toregard.repository;

import net.toregard.model.Kort;

import java.util.List;

public interface KortStokker {
    public List<Kort> sorter(Integer kortStokkId);
    public List<Kort> stokk(Integer kortStokkId);
    public Kort trekk(Integer kortStokkId);
}
