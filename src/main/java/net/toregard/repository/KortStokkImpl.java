package net.toregard.repository;

import net.toregard.fault.BusinessFault;
import net.toregard.model.Kort;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * LinkedList støtter Deque (LIFO) og Deque shuffle og brukes derfor her
 *
 */
public class KortStokkImpl implements KortStokk {
    private LinkedList<Kort> lifoListe;


    public KortStokkImpl() {
        lifoListe = new LinkedList<Kort>();
        int p = 0;
        for (int kortType = 0; kortType < 4; kortType++) {
            for (int korNummer = 0; korNummer < 13; korNummer++) {
                lifoListe.push(new Kort(kortType, korNummer));
                p++;
            }
        }
    }

    @Override
    public Deque<Kort> getDeque() {
        return lifoListe;
    }

    @Override
    public List<Kort> getList() {
        return lifoListe;
    }

    @Override
    public List<Kort> sorter() {
        Collections.sort(lifoListe, new KortComparator());
        return lifoListe;
    }

    @Override
    public List<Kort> stokk() {
        Collections.shuffle(lifoListe);
        return lifoListe;
    }

    @Override
    public Kort trekk() {
       return lifoListe.pop();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KortStokkImpl kortStokk = (KortStokkImpl) o;
        return Objects.equals(lifoListe, kortStokk.lifoListe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lifoListe);
    }
}