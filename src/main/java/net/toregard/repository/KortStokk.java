package net.toregard.repository;

import net.toregard.model.Kort;

import java.util.Deque;
import java.util.List;


public interface KortStokk {
    public Deque<Kort> getDeque();
    public List<Kort> getList();
    public List<Kort> sorter();
    public List<Kort> stokk();
    public Kort trekk();

}
