package net.toregard.fault;

public class BusinessFault extends Exception{
    public static final int STOKK_VALIDERING_KORTNUMMERID_KODE=1;
    public static final String STOKK_VALIDERING_KORTNUMMERID_MELDING="Antall kort kan ikke være mindre enn 1";
    public static final int SORTER_VALIDERING_KORTNUMMERID_KODE=2;
    public static final String SORTER_VALIDERING_KORTNUMMERID_MELDING="Antall kort kan ikke være mindre enn 1";
    public static final int TREKK_VALIDERING_KORTNUMMERID_KODE=3;
    public static final String TREKK_VALIDERING_KORTNUMMERID_MELDING="Antall kort kan ikke være mindre enn 1";

    public static final int STOKK_EMPTY_KORT_KODE=50;
    public static final String STOKK_EMPTY_KORT_MELDING="Ingen kort igjen";
    public static final int SORTER_EMPTY_KORT_KODE=51;
    public static final String SORTER_EMPTY_KORT_MELDING="Ingen kort igjen";



    public static final int TREKK_EXCEPTION_KORTNUMMERID_KODE=100;
    public static final String TREKK_EXCEPTION_KORTNUMMERID_MELDING="Ingen kort igjen";

    private int id;

    public BusinessFault(int id, String message) {
        super(message);
        this.id=id;
    }

    public int getId() {
        return id;
    }
}
