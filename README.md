# iStone Programmeringsoppgave
######av Tore Gard Andersen

###**Oppsummering av løsning**
1. Kortspillet kan bestå av en eller flere kortstokker
  * Oppgi nummerisk verdi på korstokk
  * Hvis ikke kortstokk finnes opprettes en kortstokk på oppgitt nummer

2. Sorteringen skal sortere kortene i kortspillet i henhold til farge og verdi.
  * Hjerte(rød),Ruter(Rød), Kløver(Svart), Spar(Svart). 
  * Siden "ess" kom først i rekkefølgende og ikke tallet 1, så valgte jeg å benytte forskjellige modeller for 'Kort' entitet internt og i presentasjonslaget. Intern modell har type 0-3 (hjerte, ruter, kløver, spar), og verdi(0-12). Sortering blir lettere å kode.
  * Sorteringen gjøres ikke på presentasjonslaget, men i hovedlogikk. Så sorter og stokk operasjoner endrer intern kortstokk tilstand hver for seg.

3. «Dra kort» skal vise det øverste kortet i kortspillet inntil alle kortene er trukket.
  * Man vil i praksis se øverste korstokken også visuelt i netleseren

4. Stokkingen skal legge kortene i kortspillet tilfeldig
  * Default Shuffel Java algoritme

5. Java, Tomcat server og Spring rammeverk
  * Java 1.7+, Embedded Tomcat, Spring Boot

###**Hva man må han installert på test maskin**
- git kildekontroll (github)
- gradle 2.4/2.5 byggscript 
- Det er en forutsetning at git,gradle og minst java 1.7 er installert på maskinen din.

###**Begrensninger i oppgaven**
- Enkel valideringsstrategi og exception strategi. Validerer for at antall kort ikke kan være mindre enn 1.
- Ingen logging strategi
- Kjører løsning i et Spring Boot miljø og i en embedded tomcat.

Løsningen testes via nettleser og rest kall. Du må oppgi {kortstokk nummer} som
inngangsparameter. Dette er alltid en nummerisk verdi
Eks.:
 - http://localhost:8081/kortspill/stokk/{kortstokk nummer}
 - http://localhost:8081/kortspill/sorter/{kortstokk nummer}
 - http://localhost:8081/kortspill/trekkekort/{kortstokk nummer}

Svar returneres som JSON formattert tekst.

De første json returparametere går igjen i hver respons for å gi
en potensiell klient mulighet til å handtere eventuelt feilmeldinger:

Eks. retur og forklaring:
```
{
"id": 1,                    Forklaring: id=0 er alt gått bra. id>0 er feil
"message": "Ok",            Forklaring: message er melding til klient
"kortStokkId": 1,           Forklaring: viser gjeldene kortstokk
"kort": {
            "farge": "Hjerter",
            "verdi": "ess"
        }
}
```

###**Nedlasting og kjøring**
For å laste ned og kjøre løsning lokalt gjør man følgende:

1. Plasser deg i en ønsket disk katalog
2. Skriv git kommandoen
   ```git clone https://github.com/dragerot/istoneoppgave.git```
3. Prosjektet lagres i underkatalog 'istoneoppgave'. Plasser deg der
3. For å verifiser at prosjektet bygger, skriv gradle kommando:
   ```gradle build``` eller  ```gradle test``
4. For å kjøre prosjektet skriv:
   ```gradle bootRun```
5. Alternativ (det finnes mange), kan man starte en embeded tomcat pakket inn i en WAR fil:
   ```gradle bootRepackage```
   ```java -jar ./build/libs/istoneoppgave-1.0.war```

Testing kan gjørs i nettleser. For eks. ønsker man å operere på kortstokk 1:
- http://localhost:8081/kortspill/stokk/1
- http://localhost:8081/kortspill/sorter/1
- http://localhost:8081/kortspill/trekkekort/1


Jeg har kjørende en versjon på en maskin jeg har privat.
- http://62.122.255.244:8081/kortspill/stokk/1
- http://62.122.255.244:8081/kortspill/sorter/1
- http://62.122.255.244:8081/kortspill/trekkekort/1

##**Oppbygning av kode**
***Prosjektet er delt opp i tre logiske deler***

Benytter begreper i Spring rammeverket i løsningen. 
Spring Controller, Service, Repository og Entity design pattern

######Controller######
Ansvaret for å eksponere tjeneste ut mot forbruker av tjenesten (kanal). 
Her bestemmes kommunikasjonsprotokoll (rest), grensesnitt kontrakten  transformasjon gjøres mot 
service laget pr operasjon. Kontraktens responsobjekt transformeres til JSON format.
I min verden kalles dette laget "Business service layer" 

######Service######
Service laget eksponerer sorter, trekk og stokk funksjonaliteten til Controller laget, og er ment å gjenbrukes
av 1 eller flere controllere (kanaler) i fremtiden. 

######Repository######
Dette laget holder på kort tilstanden og utføre faktisk funskjonalitet for sorter, trekk og stokk funskjonaliten.

######Entity######
Modellen  ```Kort ``` som beskriver en kortstokk.

***Nøkkel momenter i Spring løsningen***
- Jeg benytter Spring Boot rammeverket for å få fokus på løsning og ikke teknologi. 
- Benytter Spring Container annotation @Service og @Repository (som default har scope singleton). Spring Container vil alltid bare ha en instans av  KortStokkerRepository (som holder tilstanden til kortstokk). 
- Bruker Spring  @Autowired og ett interface ```net.toregard.repository.KortStokker```, for å sikre en løs binding mellom service laget og ```net.toregard.service.KortSpillServiceImpl``` og ```net.toregard.repository.KortStokkerRepository```. Ønsker man å benytte en annen ```net.toregard.repository.KortStokker``` implementasjon, så er det tatt høyde for dette.
- Sorteringen benytter ```net.toregard.repository.KortComparator``` for å sorterte en sammensatt sorteringnøkkel for Kort entitet. Kort entiten implementerer altså ikke ```Comparable``` Java interfacet.
- Default Shuffel Java algoritmen (Fisher-Yates Shuffle) i ```Collections.shuffle```benyttes for stokking av kort. Jeg har ikke funnet ut noe som tilsier at denne ikke er grei nok å bruke. 

##**Videre arbeide**

######Cluster problematikk######
Denne løsningen er ikke god nok for å kjøre i et kluster, da korstokken i denne løsningen lagrer tilstanden i hukommelsen. Løsningen kan være å innføre distribuert cache, som f.eks ehcache ([www.ehcache.org/]), Redis ([http://redis.io/]) memory cache eller kanskje en database! 
Repostory klassen KortStokkerRepository foreslår jeg bør ta ansvaret for dette.

######Hosting######
Siden jeg har valgt Spring Boot, så er løsningen klar for å legges i en container basert teknologi som Docker (https://www.docker.com/). Open Shift eller Azure er da mulige hosting leverandører for løsningen.

Jeg har lagt til en gradle task med navnet ```buildDocker``` i build.gradle skriptet. Task produserer et docker image som
kan kjøre løsningen i en Docker container (tomcat embedded). En Dockerfile ligger som grunnlag
for buildDocker tasken, se ```/src/main/docker/Dockerfile```.

Kommandoer for å starte opp en docker container:

- ```sudo gradle buildDocker```
lager et image med navn: toregard/istoneoppgave
- ```sudo docker run -p 8081:8081 -t -d <imageid>```

Et kjørbart eksempel, f.eks http://62.122.255.244:8081/kortspill/stokk/1

######Feilhandtering######
Forbedre feilhandtering. Målet å være å minst mulig feil handtering dypt ned i koden, og hellere la feilhandteringen boble opp til et sentralt punkt. Service laget kan gjøre dette. Controller laget bør ta ansvaret for å ha ressurser filer 
på forskjellige språk og transformerer spesifikk feilhandtering per kanal.

######Logging######
Har ikke sett noe på dette. Bør bygges rundt  http://www.slf4j.org/ rammeverket, som tilbyr en fasade abstraksjon for plugin av logging implementasjoner.

######Kjente feil######
Bedre feilhandtering når man ikke benytter nummerisk heltatt i kortnummer.
